<?php
namespace NavinLab\LaravelMsi\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @see \NavinLab\LaravelMsi\MsiManager
 */
class Msi extends Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor() {
        return 'msi';
    }
}