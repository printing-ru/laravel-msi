<?php
/**
 * laravel-msi.
 * Date: 27/06/17
 * Time: 18:09
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace NavinLab\LaravelMsi;


use Illuminate\Support\Arr;
use InvalidArgumentException;
use NavinLab\LaravelMsi\Contracts\Factory;
use NavinLab\LaravelMsi\Contracts\Registry;
use NavinLab\LaravelMsi\Contracts\Service;
use NavinLab\LaravelMsi\Services\GraphqlService;
use NavinLab\LaravelMsi\Services\JsonrpcService;
use NavinLab\LaravelMsi\Services\RestService;
use NavinLab\LaravelMsi\Services\SimpleService;

class MsiServiceManager extends MsiManager
{
    /**
     * Get the filesystem connection configuration.
     *
     * @param  string $name
     * @param array $options
     * @return array
     */
    protected function getConfig($name, $options = [])
    {
        $config = $this->app['config'];
        return array_merge_recursive(
            $options,
            $config->get("msi.services.{$name}", []),
            ['middleware' => $config->get('msi.middleware', [])],
            $config->get('msi.options', [])
        );
    }

    /**
     * Get the default driver name.
     *
     * @return string
     */
    public function getDefaultService()
    {
        return $this->app['config']['msi.default'];
    }

    /**
     * @param $config
     * @return Registry
     */
    protected function getRegistry($config) {
        return $this->app['msi.registry']->service(Arr::get($config, 'registry', 'default'));
    }

    /**
     * @param $config
     * @return JsonrpcService
     */
    protected function createJsonrpcService($config) {
        return new JsonrpcService($this->app, $config);
    }

    /**
     * @param $config
     * @return RestService
     */
    protected function createRestService($config) {
        return new RestService($this->app, $config);
    }

    /**
     * @param $config
     * @return SimpleService
     */
    protected function createSimpleService($config) {
        return new SimpleService($this->app, $config);
    }

    /**
     * @param $config
     * @return GraphqlService
     */
    protected function createGraphqlService($config) {
        return new GraphqlService($this->app, $config);
    }

    /**
     * Resolve the given service.
     *
     * @param  string $name
     *
     * @param array $options
     * @return Service
     */
    protected function resolve($name, $options = [])
    {
        $config = $this->getConfig($name, $options);
        $serviceMethod = 'create'.ucfirst($type = Arr::pull($config, 'type', 'unknown')).'Service';
        if (method_exists($this, $serviceMethod)) {
            $service = Arr::get($config, 'service', $name);
            //get $config['base_uri'] from service discovery
            $meta = $this->getRegistry($config)->healthy($service, $config);
            $config['base_uri'] = $meta->getBaseUri();

            return $this->{$serviceMethod}($config);
        } else {
            throw new InvalidArgumentException("Service type [{$type}] is not supported.");
        }
    }

}