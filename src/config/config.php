<?php
/**
 * laravel-msi
 * Date: 27/06/17
 * Time: 15:21
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */
return [
    /*
    |--------------------------------------------------------------------------
    | Default Msi middleware
    |--------------------------------------------------------------------------
    |
    | Laravel's msi API supports a middleware.
    |
    */
    'middleware' => [
        \NavinLab\LaravelMsi\Middleware\Localization::class,
    ],
    /*
    |--------------------------------------------------------------------------
    | Default Msi options
    |--------------------------------------------------------------------------
    |
    | Laravel's msi API supports a middleware.
    |
    */
    'options' => [],

    /*
    |--------------------------------------------------------------------------
    | Default Registry  = Service Discovery
    |--------------------------------------------------------------------------
    |
    | Laravel's msi API supports a registry.
    |
    | null do not use service discovery
    */
    'registry' => 'default',
    /*
    |--------------------------------------------------------------------------
    | Registry types = Service Discovery types
    |--------------------------------------------------------------------------
    |
    */
    'registries' => [
        'default' => [
            'type' => 'config',
        ],
        'consul' => [
            'type' => 'consul',
            'base_uri' => '127.0.0.1:8500',
            'connection_timeout' => 3,
            'timeout' => 3,
        ],
    ],
    /*
    |--------------------------------------------------------------------------
    | Default Msi service
    |--------------------------------------------------------------------------
    |
    |
    */
    'default' => 'rest',

    /*
    |--------------------------------------------------------------------------
    | Msi services
    |--------------------------------------------------------------------------
    |
    | Here you may configure the connection information for each service that
    | is used by your application. A default configuration has been added
    | for each type. You are free to add more.
    |
    */
    'services' => [
        'httpbin' => [
            'type' => 'simple',
            'base_uri' => 'https://httpbin.org/',
        ],
        'simple' => [
            'type' => 'simple',
            'base_uri' => '',
        ],
        'rest' => [
            'type' => 'rest',
            'base_uri' => '',
        ],
        'github' => [
            'type' => 'graphql',
            'base_uri' => '',
        ],
        'jsonrpc' => [
            'type' => 'jsonrpc',
            'registry' => 'consul',
            'service' => 'jsonrpc',
        ],
    ],
];