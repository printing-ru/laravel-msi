<?php
/**
 * laravel-msi.
 * Date: 27/06/17
 * Time: 18:54
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace NavinLab\LaravelMsi\Middleware;


use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

abstract class Middleware
{
    /**
     * @param RequestInterface $request
     * @param array $option
     * @return RequestInterface
     */
    public function request(RequestInterface $request, array $option)
    {
        return $request;
    }

    /**
     * @param ResponseInterface $response
     * @param array $options
     * @return mixed
     */
    public function response(ResponseInterface $response, array $options)
    {
        return $response;
    }
}