<?php
/**
 * laravel-msi.
 * Date: 27/06/17
 * Time: 17:37
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace NavinLab\LaravelMsi\Middleware;


use Illuminate\Foundation\Application;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;
use Psr\Http\Message\ResponseInterface;

class Panic extends Middleware
{
    /**
     * @var Application
     */
    private $app;

    /**
     * Localization constructor.
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * @param ResponseInterface $response
     * @param array $options
     * @return mixed
     */
    public function response(ResponseInterface $response, array $options)
    {
        if ($response->getStatusCode() >= 400) {
            throw new HttpResponseException(new Response($response->getBody(), $response->getStatusCode(), $response->getHeaders()));
        }
        return $response;
    }
}