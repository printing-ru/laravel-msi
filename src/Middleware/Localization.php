<?php
/**
 * laravel-msi.
 * Date: 27/06/17
 * Time: 17:37
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace NavinLab\LaravelMsi\Middleware;


use Illuminate\Foundation\Application;
use Psr\Http\Message\RequestInterface;

class Localization extends Middleware
{
    /**
     * @var Application
     */
    private $app;

    /**
     * Localization constructor.
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * @inheritdoc
     */
    public function request(RequestInterface $request, array $options)
    {
        $request->withHeader('Accept-language', $this->app->getLocale());
    }
}