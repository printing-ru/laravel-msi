<?php
/**
 * laravel-msi.
 * Date: 27/06/17
 * Time: 16:47
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace NavinLab\LaravelMsi\Registries;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Arr;
use NavinLab\LaravelMsi\Contracts\Registry;
use NavinLab\LaravelMsi\Registry\ConsulMeta;
use SensioLabs\Consul;

class ConsulRegistry implements Registry
{
    /**
     * @var Application
     */
    protected $app;
    /**
     * @var mixed
     */
    protected $health;

    /**
     * RestService constructor.
     * @param Application $app
     * @param array $config
     */
    public function __construct(Application $app, $config = [])
    {
        $this->app = $app;
        $this->config = $config;
        $this->health =  (new Consul\ServiceFactory($config))->get('health');
    }

    /**
     * @param $name
     * @param $options
     * @return mixed
     * @throws \Exception
     */
    public function healthy($name, $options = []) {
        $available = $this->available($name, $options, true);
        if (count($available) === 0) {
            throw new \Exception(sprintf('Healthy service %s not exists', $options['service']));
        }
        list($healthy) = $available;
        return $healthy;
    }

    /**
     * @param $name
     * @param $options
     * @param bool $onlyHealthy
     * @return array
     * @throws \Exception
     */
    public function available($name, $options = [], $onlyHealthy = false) {
        //get all info
        $response = $this->health->service($name);

        $services = \json_decode($response->getBody());
        if (!$services) {
            throw new \Exception(sprintf('Service %s not exists', $name));
        }
        //accumulate
        $metas = [];
        foreach ($services as $service) {
            $meta = new ConsulMeta($service);
            if ($onlyHealthy && !$meta->isHealthy()) {
                continue;
            }
            array_push($metas, $meta);
        }
        return $metas;
    }


}