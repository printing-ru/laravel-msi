<?php
/**
 * laravel-msi.
 * Date: 27/06/17
 * Time: 16:47
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace NavinLab\LaravelMsi\Registries;

use Illuminate\Contracts\Foundation\Application;
use NavinLab\LaravelMsi\Contracts\Registry;
use NavinLab\LaravelMsi\Registry\ConfigMeta;

class ConfigRegistry implements Registry
{
    /**
     * @var Application
     */
    protected $app;

    /**
     * RestService constructor.
     * @param Application $app
     * @param array $config
     */
    public function __construct(Application $app, $config = [])
    {
        $this->app = $app;
    }

    /**
     * @param $name
     * @param $options
     * @return mixed
     */
    public function healthy($name, $options = []) {
        return new ConfigMeta($name, $options);
    }

    /**
     * @param $name
     * @param array $options
     * @param bool $onlyHealthy
     * @return array
     */
    public function available($name, $options = [], $onlyHealthy = false) {
        return [$this->healthy($name, $options)];
    }
}