<?php
/**
 * laravel-msi.
 * Date: 27/06/17
 * Time: 18:09
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace NavinLab\LaravelMsi;


use Illuminate\Support\Arr;
use InvalidArgumentException;
use NavinLab\LaravelMsi\Contracts\Factory;
use NavinLab\LaravelMsi\Contracts\Service;
use NavinLab\LaravelMsi\Services\GraphqlService;
use NavinLab\LaravelMsi\Services\RestService;
use NavinLab\LaravelMsi\Services\SimpleService;

abstract class MsiManager implements Factory
{
    /**
     * The application instance.
     *
     * @var \Illuminate\Contracts\Foundation\Application
     */
    protected $app;

    /**
     * The array of resolved ms services.
     *
     * @var array
     */
    protected $services = [];

    /**
     * Create a new msi manager instance.
     *
     * @param  \Illuminate\Contracts\Foundation\Application $app
     */
    public function __construct($app)
    {
        $this->app = $app;
    }

    /**
     * Get an amqp instance.
     *
     * @param  string $name
     * @param array $options
     * @return Service
     */
    public function service($name = null, $options = [])
    {
        $name = $name ?: $this->getDefaultService();
        return $this->services[$name] = $this->get($name, $options);
    }

    /**
     * Attempt to get the service
     *
     * @param  string $name
     * @param array $options
     * @return Service
     */
    protected function get($name, $options = [])
    {
        return isset($this->services[$name]) ? $this->services[$name] : $this->resolve($name, $options);
    }

    /**
     * Resolve the given service.
     *
     * @param  string $name
     *
     * @param array $options
     * @return Service
     */
    abstract protected function resolve($name, $options = []);

    /**
     * Get the filesystem connection configuration.
     *
     * @param  string $name
     * @param array $options
     * @return array
     */
    abstract protected function getConfig($name, $options = []);

    /**
     * Get the default driver name.
     *
     * @return string
     */
    abstract protected function getDefaultService();

    /**
     * Dynamically call the default driver instance.
     *
     * @param  string  $method
     * @param  array   $parameters
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        return $this->service()->$method(...$parameters);
    }
}