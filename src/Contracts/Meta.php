<?php
/**
 * laravel-msi.
 * Date: 27/06/17
 * Time: 18:24
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace NavinLab\LaravelMsi\Contracts;


interface Meta
{
    /**
     * Get unique ID of the service
     * @return mixed
     */
    public function getID();

    /**
     * Get Name of the service
     * @return mixed
     */
    public function getName();

    /**
     * Get base uri to request the service
     * @return mixed
     */
    public function getBaseUri();

    /**
     * Get all meta info fromservice discovery
     * @return mixed
     */
    public function getMeta();

    /**
     * Get healthy status
     * true - healthy
     * false - critical issue
     * @return mixed
     */
    public function isHealthy();
}