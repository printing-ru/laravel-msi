<?php
/**
 * laravel-msi.
 * Date: 27/06/17
 * Time: 18:24
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace NavinLab\LaravelMsi\Contracts;


interface Registry
{
    /**
     * @param $name
     * @param $options
     * @return Meta
     */
    public function healthy($name, $options);

    /**
     * @param $name
     * @param $options
     * @param bool $onlyHealthy
     * @return  []Meta
     */
    public function available($name, $options, $onlyHealthy = false);
}