<?php
/**
 * laravel-msi.
 * Date: 27/06/17
 * Time: 18:24
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace NavinLab\LaravelMsi\Contracts;


interface Service
{
    /**
     * @param array ...$middlewares
     * @return static
     */
    public function middleware(...$middlewares);

    /**
     * @return mixed
     */
    public function handler();
}