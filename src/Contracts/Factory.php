<?php
/**
 * laravel-msi.
 * Date: 27/06/17
 * Time: 12:22
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace NavinLab\LaravelMsi\Contracts;


interface Factory
{
    /**
     * Service resolver
     *
     * @param null $name
     * @return mixed
     */
    public function service($name = null);
}