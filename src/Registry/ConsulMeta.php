<?php
/**
 * Created by PhpStorm.
 * User: navin
 * Date: 30/05/2018
 * Time: 13:48
 */

namespace NavinLab\LaravelMsi\Registry;


use NavinLab\LaravelMsi\Contracts\Meta;

class ConsulMeta implements  Meta
{

    /**
     * @var \stdClass
     */
    protected $meta;
    /**
     * @var
     */
    protected $healthy = true;

    /**
     * ServiceMeta constructor.
     * @param array $meta
     */
    public function __construct($meta)
    {
        $this->meta = $meta;
        $this->init();
    }

    /**
     * @return mixed
     */
    public function getBaseUri()
    {
        return sprintf('%s:%d', $this->meta->Service->Address, $this->meta->Service->Port);
    }

    /**
     * @return bool
     */
    public function isHealthy() {
        return $this->healthy;
    }

    /**
     * @return null
     */
    public function getMeta()
    {
        return $this->meta;
    }

    /**
     * Init healthy status
     */
    protected function init()
    {
        foreach ($this->meta->Checks as $idx => $check) {
            if ($check->Status == 'critical') {
                $this->healthy = false;
                break;
            }
        }
    }

    /**
     * @return mixed
     */
    public function getID()
    {
        return $this->meta->Service->ID;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->meta->Service->Service;
    }
}