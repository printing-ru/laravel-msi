<?php
/**
 * Created by PhpStorm.
 * User: navin
 * Date: 30/05/2018
 * Time: 13:48
 */

namespace NavinLab\LaravelMsi\Registry;


use Illuminate\Support\Arr;
use NavinLab\LaravelMsi\Contracts\Meta;

class ConfigMeta implements  Meta
{
    /**
     * @var array
     */
    protected $meta;
    /**
     * @var
     */
    protected $name;

    /**
     * ServiceMeta constructor.
     * @param $name
     * @param array $meta
     * @internal param $base_uri
     */
    public function __construct($name, $meta)
    {
        $this->meta = $meta;
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getBaseUri()
    {
        return Arr::get($this->meta, 'base_uri', '');
    }

    public function isHealthy() {
        return true;
    }

    /**
     * @return null
     */
    public function getMeta()
    {
        return $this->meta;
    }

    public function getID()
    {
        return $this->getName();
    }

    public function getName()
    {
        return $this->name;
    }
}