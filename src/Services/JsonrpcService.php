<?php
/**
 * laravel-msi.
 * Date: 27/06/17
 * Time: 15:26
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace NavinLab\LaravelMsi\Services;

use Graze\GuzzleHttp\JsonRpc;
use Graze\GuzzleHttp\JsonRpc\Message\MessageFactory;
use Illuminate\Contracts\Foundation\Application;

class JsonrpcService extends BaseService
{
    /**
     * @var JsonRpc\Client
     */
    protected $rpcClient;

    /**
     * JsonrpcService constructor.
     * @param Application $app
     * @param array $config
     */
    public function __construct(Application $app, array $config = [])
    {
        parent::__construct($app, $config);
        $this->rpcClient = new JsonRpc\Client($this->client, new MessageFactory());
    }

    /**
     * @param $method
     * @param array $params
     * @param bool $async
     * @return \GuzzleHttp\Promise\PromiseInterface|\Psr\Http\Message\ResponseInterface
     */
    public function request($method, $params = [], $async = false)
    {
        $id = uniqid();
        $request = $this->rpcClient->request($id, $method, $params);
        return $async
            ? $this->rpcClient->sendAsync($request)
            : $this->rpcClient->send($request);
    }

    /**
     * @param $method
     * @param array $params
     * @param bool $async
     * @return \GuzzleHttp\Promise\PromiseInterface|\Psr\Http\Message\ResponseInterface
     */
    public function notification($method, $params = [], $async = false)
    {
        $notification = $this->rpcClient->notification($method, $params);
        return $async
            ? $this->rpcClient->sendAsync($notification)
            : $this->rpcClient->send($notification);
    }
}