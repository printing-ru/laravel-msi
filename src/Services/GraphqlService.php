<?php
/**
 * laravel-msi.
 * Date: 27/06/17
 * Time: 15:26
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace NavinLab\LaravelMsi\Services;

/**
 * Class GraphqlService
 * @package NavinLab\LaravelMsi\Services
 * @see http://graphql.org/learn/serving-over-http/
 */
class GraphqlService extends BaseService
{
    /**
     * @param $graphql
     * @param array $variables
     * @param bool $async
     * @param array $options
     * @return \GuzzleHttp\Promise\PromiseInterface|\Psr\Http\Message\ResponseInterface
     */
    public function query($graphql, $variables = [], $async = false, $options = [])
    {
        $options = array_merge_recursive($options, [
            'query' => [
                'query' => $this->build($graphql),
                'variables' => $variables,
            ],
        ]);

        return $async
            ? $this->client->getAsync($this->uri(), $options)
            : $this->client->get($this->uri(), $options);
    }

    /**
     * @param $graphql
     * @param array $variables
     * @param bool $async
     * @param array $options
     * @return \GuzzleHttp\Promise\PromiseInterface|\Psr\Http\Message\ResponseInterface
     */
    public function pquery($graphql, $variables = [], $async = false, $options = [])
    {
        $options = array_merge_recursive($options, [
            'json' => [
                'query' => $this->build($graphql),
                'variables' => $variables,
            ],
        ]);

        return $async
            ? $this->client->postAsync($this->uri(), $options)
            : $this->client->post($this->uri(), $options);
    }

    /**
     * @param $graphql
     * @param array $variables
     * @param bool $async
     * @param array $options
     * @return \GuzzleHttp\Promise\PromiseInterface|\Psr\Http\Message\ResponseInterface
     */
    public function mutation($graphql, $variables = [], $async = false, $options = [])
    {
        $options = array_merge_recursive($options, [
            'json' => [
                'query' => $this->build($graphql),
                'variables' => $variables,
            ],
        ]);
        return $async
            ? $this->client->postAsync($this->uri(), $options)
            : $this->client->post($this->uri(), $options);
    }

    /**
     * @param $graphql
     * @return bool|resource
     */
    protected function build($graphql)
    {
        if (is_file($graphql)) {
            return file_get_contents($graphql);
        }
        return $graphql;
    }
}