<?php
/**
 * laravel-msi.
 * Date: 28/06/17
 * Time: 07:08
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace NavinLab\LaravelMsi\Services;


use Illuminate\Support\Arr;

class SimpleService extends BaseService
{
    /**
     * @var string
     */
    protected $uri = '';

    /**
     * Get request uri and clean it
     * @return string
     */
    protected function getUriAndClean()
    {
        $uri = $this->uri();
        $this->uri = '';
        return $uri;
    }

    /**
     * @param array $query
     * @param bool $async
     * @param array $options
     * @return \GuzzleHttp\Promise\PromiseInterface|\Psr\Http\Message\ResponseInterface
     */
    public function get($query = [], $async = false, $options = [])
    {
        $options = array_merge($options, compact('query'));
        return $async
            ? $this->client->getAsync($this->getUriAndClean(), $options)
            : $this->client->get($this->getUriAndClean(), $options);
    }

    /**
     * @param array $json
     * @param bool $async
     * @param array $options
     * @return \GuzzleHttp\Promise\PromiseInterface|\Psr\Http\Message\ResponseInterface
     */
    public function post($json = [], $async = false, $options = [])
    {
        $options = array_merge($options, compact('json'));
        return $async
            ? $this->client->postAsync($this->getUriAndClean(), $options )
            : $this->client->post($this->getUriAndClean(), $options);
    }

    /**
     * @param array $json
     * @param bool $async
     * @param array $options
     * @return \GuzzleHttp\Promise\PromiseInterface|\Psr\Http\Message\ResponseInterface
     */
    public function put($json = [], $async = false, $options = [])
    {
        $options = array_merge($options, compact('json'));
        return $async
            ? $this->client->putAsync($this->getUriAndClean(), $options)
            : $this->client->put($this->getUriAndClean(), $options);
    }

    /**
     * @param array $json
     * @param bool $async
     * @param array $options
     * @return \GuzzleHttp\Promise\PromiseInterface|\Psr\Http\Message\ResponseInterface
     */
    public function patch($json = [], $async = false, $options = [])
    {
        $options = array_merge($options, compact('json'));
        return $async
            ? $this->client->patchAsync($this->getUriAndClean(), $options)
            : $this->client->patch($this->getUriAndClean(), $options);
    }

    /**
     * @param bool $async
     * @param array $options
     * @return \GuzzleHttp\Promise\PromiseInterface|\Psr\Http\Message\ResponseInterface
     */
    public function delete($async = false, $options = [])
    {
        return $async
            ? $this->client->deleteAsync($this->getUriAndClean(), $options)
            : $this->client->delete($this->getUriAndClean(), $options);
    }

    /**
     * @param $name
     * @param $arguments
     * @return $this
     */
    public function __call($name, $arguments)
    {
        $glue = Arr::get($arguments, 2, Arr::get($this->config, 'transform_name', '-'));
        return $this->route(...array_merge([$this->transformName($name, $glue)], $arguments));
    }

    /**
     * @param $method
     * @param null $id
     * @param string $ends
     * @return $this
     */
    public function route($method, $id = null, $ends = '') {
        $this->uri .= sprintf('%s%s', '' === $this->uri ? '' : '/', $method);
        //add id if need
        if (!is_null($id)) {
            $this->uri .= sprintf('/%s', $id);
        }
        //finish route
        $this->uri .= Arr::get($this->config, 'route_end', $ends);
        return $this;
    }

    /**
     * Trasnform name with glue
     * @param $input
     * @param string $glue
     * @return string
     */
    protected function transformCamelCase($input, $glue = '-') {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
        list($ret) = $matches;
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        return implode($glue, $ret);
    }

    /**
     * Transform name if need
     * @param $name
     * @param null $type
     * @return string
     */
    protected function transformName($name, $type = null)
    {
        return '' === $type ? $name : $this->transformCamelCase($name, $type);
    }
}