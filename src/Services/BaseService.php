<?php
/**
 * laravel-msi.
 * Date: 27/06/17
 * Time: 16:47
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace NavinLab\LaravelMsi\Services;

use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Arr;
use NavinLab\LaravelMsi\Contracts\Service;
use NavinLab\LaravelMsi\Middleware\Middleware;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

abstract class BaseService implements Service
{
    /**
     * @var Client
     */
    protected $client;
    /**
     * @var HandlerStack
     */
    protected $handler;
    /**
     * @var string
     */
    protected $uri = '';

    /**
     * @var array
     */
    protected $config;
    /**
     * @var Application
     */
    protected $app;

    /**
     * RestService constructor.
     * @param Application $app
     * @param array $config
     */
    public function __construct(Application $app, $config = [])
    {
        //set handler to push middlewares
        if (!Arr::has($config, 'handler')) {
            $config['handler'] = new HandlerStack(\GuzzleHttp\choose_handler());
        }
        $this->handler = $config['handler'];
        $this->config = $config;
        $this->app = $app;

        $this->middleware(...Arr::pull($this->config, 'middleware', []));

        //init guzzle client
        $this->client = new Client($config);
    }

    /**
     * @return mixed
     */
    public function uri() {
        return $this->uri;
    }
    /**
     * @return HandlerStack
     */
    public function handler()
    {
        return $this->handler;
    }

    /**
     * Register middleware
     * @param array ...$middlewares
     * @return $this
     */
    public function middleware(...$middlewares) {
        foreach ($middlewares as $middleware) {
            $middleware = $this->app->make($middleware);

            if ($middleware instanceof Middleware) {
                $this->handler->push(function (callable $handler) use ($middleware) {
                    return function (RequestInterface $request, array $options) use ($handler, $middleware) {


                        $this->app->call([$middleware, 'request'], compact('request', 'options'));

                        return $handler($request, $options)->then(function (ResponseInterface $response) use($middleware, $options) {
                            return $this->app->call([$middleware, 'response'], compact('response', 'options'));
                        });
                    };
                });
            } else {
                $this->handler->push($middleware);
            }

        }
        return $this;
    }
}