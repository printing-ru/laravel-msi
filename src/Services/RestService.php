<?php
/**
 * laravel-msi.
 * Date: 27/06/17
 * Time: 15:26
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace NavinLab\LaravelMsi\Services;

class RestService extends SimpleService
{
    /**
     * @param array $json
     * @param bool $async
     * @param array $options
     * @return \GuzzleHttp\Promise\PromiseInterface|\Psr\Http\Message\ResponseInterface
     */
    public function store($json = [], $async = false, $options = [])
    {
        return $this->post($json, $async, $options);
    }

    /**
     * @param array $json
     * @param bool $async
     * @param array $options
     * @return \GuzzleHttp\Promise\PromiseInterface|\Psr\Http\Message\ResponseInterface
     */
    public function update($json = [], $async = false, $options = [])
    {
        return $this->put($json, $async, $options);
    }
}