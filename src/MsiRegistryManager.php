<?php
/**
 * laravel-msi.
 * Date: 27/06/17
 * Time: 18:09
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace NavinLab\LaravelMsi;


use Illuminate\Support\Arr;
use NavinLab\LaravelMsi\Contracts\Registry;
use NavinLab\LaravelMsi\Contracts\Service;
use NavinLab\LaravelMsi\Registries\ConfigRegistry;
use NavinLab\LaravelMsi\Registries\ConsulRegistry;

class MsiRegistryManager extends MsiManager
{
    /**
     * Get the filesystem connection configuration.
     *
     * @param  string $name
     * @param array $options
     * @return array
     */
    protected function getConfig($name, $options = [])
    {
        return $this->app['config']->get("msi.registries.{$name}", []);
    }

    /**
     * Get the default driver name.
     *
     * @return string
     */
    public function getDefaultService()
    {
        return $this->app['config']['msi.registry'];
    }

    /**
     * @param $config
     * @return Registry
     */
    protected function createConfigRegistry($config) {
        return new ConfigRegistry($this->app, $config);
    }

    /**
     * @param $config
     * @return Registry
     */
    protected function createConsulRegistry($config) {
        return new ConsulRegistry($this->app, $config);
    }

    /**
     * Resolve the given service.
     *
     * @param  string $name
     *
     * @param array $options
     * @return Registry
     */
    protected function resolve($name, $options = [])
    {
        $config = $this->getConfig($name, $options);
        $serviceMethod = 'create'.ucfirst($type = Arr::pull($config, 'type', 'config')).'Registry';
        if (method_exists($this, $serviceMethod)) {
            return $this->{$serviceMethod}($config);
        } else {
            return $this->createConfigRegistry($config);
        }
    }
}