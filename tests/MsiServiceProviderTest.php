<?php
namespace Tests;

use Illuminate\Support\Facades\Config;

class MsiServiceProviderTest extends TestCase
{
    /**
     * Test able to load aggregate service providers.
     *
     * @test
     */
    public function testServiceIsAvailable()
    {
        $this->assertTrue($this->app->bound('msi'));
    }

    public function testConfigIsAvailable() {
        $this->assertTrue(Config::has('msi.default'));
        $this->assertTrue(Config::has('msi.services'));
        $this->assertTrue(Config::has('msi.registries'));
        $this->assertTrue(Config::has('msi.registry'));
        $this->assertTrue(Config::has('msi.middleware'));
        $this->assertTrue(Config::has('msi.options'));
        $this->assertTrue(Config::has('msi.services.' . Config::get('msi.default')));
    }
}