<?php
namespace Tests;

use Illuminate\Support\Facades\Config;
use InvalidArgumentException;
use NavinLab\LaravelMsi\Contracts\Factory;
use NavinLab\LaravelMsi\Contracts\Service;
use NavinLab\LaravelMsi\Facades\Msi;
use NavinLab\LaravelMsi\Services\GraphqlService;
use NavinLab\LaravelMsi\Services\JsonrpcService;
use NavinLab\LaravelMsi\Services\RestService;
use NavinLab\LaravelMsi\Services\SimpleService;

class MsiServiceManagerTest extends TestCase
{
    protected function setUp()
    {
        parent::setUp();
        Config::set('msi.default', 'rest');
        Config::set('msi.services', [
            'simple' => [
                'type' => 'simple',
            ],
            'rest' => [
                'type' => 'rest',
            ],
            'graphql' => [
                'type' => 'graphql',
            ],
            'jsonrpc' => [
                'type' => 'jsonrpc',
            ],
        ]);
    }
    /**
     * Should be implementation if Factory
     */
    public function testContract() {
        $this->assertInstanceOf(Factory::class, $this->app['msi']);
    }

    /**
     * Should throw an exception
     */
    public function testServiceTypeDoesntExists() {
        $this->expectException(InvalidArgumentException::class);
        Msi::service('unknown');
    }

    /**
     * Test rest service resolve
     * @see static::setUp
     */
    public function testRestServiceExists() {
        $this->assertInstanceOf(RestService::class, Msi::service('rest'));
    }

    /**
     * Test rest service resolve
     * @see static::setUp
     */
    public function testJsonrpcServiceExists() {
        $this->assertInstanceOf(JsonrpcService::class, Msi::service('jsonrpc'));
    }

    /**
     * Test simple service resolve
     * @see static::setUp
     */
    public function testSimpleServiceExists() {
        $this->assertInstanceOf(SimpleService::class, Msi::service('simple'));
    }

    /**
     * Test graphql service resolve
     * @see static::setUp
     */
    public function testGraphqlServiceExists() {
        $this->assertInstanceOf(GraphqlService::class, Msi::service('graphql'));
    }

    /**
     * Should return same service
     */
    public function testDefaultService() {
        $this->assertInstanceOf(Service::class, $defaultService = Msi::service());
        $this->assertInstanceOf(Service::class, $service = Msi::service(Config::get('service.default')));
        $this->assertEquals($service, $defaultService);
    }
}