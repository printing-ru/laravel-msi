<?php
/**
 * laravel-msi.
 * Date: 27/06/17
 * Time: 20:10
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace Tests\Unit\Services;

use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Arr;
use NavinLab\LaravelMsi\Services\SimpleService;

class SimpleServiceTest extends ServiceTestCase
{
    /**
     * Init service
     *
     * @param $options
     * @return SimpleService
     */
    protected function getService($options) {
        return new SimpleService($this->app, $options);
    }
    /**
     * Uri building test
     */
    public function testUriCorrectBuilding() {
        $history = [];
        $input  = $this->genBody();
        $handler = $this->mockHandler($history, [
            new Response(200, [], $input),
            new Response(200, [], $input),
            new Response(200, [], $input),
            new Response(200, [], $input),
        ]);
        $base_uri = 'http://testhost/api/v1/';

        $service = $this->getService(compact('base_uri', 'handler'));

        $service->companies(1)->innovations(1)->assets()->get();
        $this->assertEquals('/api/v1/companies/1/innovations/1/assets', Arr::get($history,'0.request')->getUri()->getPath());

        $service->companies(1)->innovations(2)->assets(null, '/')->get();
        $this->assertEquals('/api/v1/companies/1/innovations/2/assets/', Arr::get($history,'1.request')->getUri()->getPath());

        $service->basicAuth(null, '', '-')->route('test', null, '/')->get();
        $this->assertEquals('/api/v1/basic-auth/test/', Arr::get($history,'2.request')->getUri()->getPath());

        $service->basicAuth2(null, '', '_')->route('test', null, '/')->get();
        $this->assertEquals('/api/v1/basic_auth2/test/', Arr::get($history,'3.request')->getUri()->getPath());
    }

    protected function universal($method, $requestMethod, $equalBody = false, $equalQuery = false) {
        $history = [];
        $data  = $this->genData();
        $handler = $this->mockHandler($history, [
            new Response(200, [], \GuzzleHttp\json_encode($data)),
            new Response(200, [], \GuzzleHttp\json_encode($data)),
        ]);
        $base_uri = 'http://testhost/api/v1/';
        $service = $this->getService(compact('base_uri', 'handler'));

        $service->$method($data);
        $this->assertUniversal($requestMethod, $equalBody, $equalQuery, $history, 0, $data);

        //async
        $service->$method($data, true)->wait();
        $this->assertUniversal($requestMethod, $equalBody, $equalQuery, $history, 1, $data);
    }

    /**
     * Get method test
     */
    public function testGet() {
        $this->universal('get', 'GET', false, true);
    }

    /**
     * Post method test
     */
    public function testPost() {
        $this->universal('post', 'POST', true, false);
    }

    /**
     * Put method test
     */
    public function testPut() {
        $this->universal('put', 'PUT', true, false);
    }

    /**
     * Patch method test
     */
    public function testPatch() {
        $this->universal('patch', 'PATCH', true, false);
    }

    /**
     * Delete method test
     */
    public function testDelete() {
        $history = [];
        $handler = $this->mockHandler($history, [
            new Response(200),
            new Response(200),
        ]);
        $base_uri = 'http://testhost/api/v1/';
        $service = $this->getService(compact('base_uri', 'handler'));

        $service->delete();
        $this->assertEquals('DELETE', Arr::get($history,'0.request')->getMethod());

        //async
        $service->delete(true)->wait();
        $this->assertEquals('DELETE', Arr::get($history,'1.request')->getMethod());
    }

    /**
     * @param $requestMethod
     * @param $equalBody
     * @param $equalQuery
     * @param $history
     * @param $data
     */
    protected function assertUniversal($requestMethod, $equalBody, $equalQuery, $history, $index, $data)
    {
        $this->assertEquals($requestMethod, Arr::get($history, $index . '.request')->getMethod());
        if ($equalBody) {
            $this->assertEquals($data, \GuzzleHttp\json_decode(Arr::get($history, $index . '.response')->getBody(), true));
        }
        if ($equalQuery) {
            $this->assertEquals(http_build_query($data), Arr::get($history, $index . '.request')->getUri()->getQuery());
        }
    }

}