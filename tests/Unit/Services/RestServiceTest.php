<?php
/**
 * laravel-msi.
 * Date: 27/06/17
 * Time: 20:10
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace Tests\Unit\Services;

use NavinLab\LaravelMsi\Services\RestService;

class RestServiceTest extends SimpleServiceTest
{
    /**
     * @inheritdoc
     */
    protected function getService($options)
    {
        return new RestService($this->app, $options);
    }

    /**
     * Test store method
     */
    public function testStore() {
        $this->universal('store', 'POST', true, false);
    }

    /**
     * Test
     */
    public function testUpdate() {
        $this->universal('update', 'PUT', true, false);
    }
}