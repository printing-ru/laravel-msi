<?php
/**
 * laravel-msi.
 * Date: 27/06/17
 * Time: 20:13
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace Tests\Unit\Services;


use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;
use Tests\TestCase;

abstract class ServiceTestCase extends TestCase
{
    /**
     * @param $options
     * @return mixed
     */
    protected abstract function getService($options);
    /**
     * Generate fake data
     */
    protected function genData() {
        return [Str::random() => rand(1, 1000), Str::random() => rand(1, 1000)];
    }

    /**
     * @return string
     */
    protected function genBody() {
        return \GuzzleHttp\json_encode($this->genData());
    }

    protected function mockHandler(&$container, $responses = []) {
        $mock = new MockHandler($responses);
        $handler = HandlerStack::create($mock);
        $handler->push(Middleware::history($container));
        return $handler;
    }

}