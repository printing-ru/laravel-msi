<?php
/**
 * laravel-msi.
 * Date: 27/06/17
 * Time: 21:06
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace Tests\Unit\Services;

use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Arr;
use NavinLab\LaravelMsi\Services\GraphqlService;

class GraphqlServiceTest extends ServiceTestCase
{
    protected function universal($method, $requestMethod, $equalBody = false, $equalQuery = false) {
        $history = [];
        $data = [
            'data' => [
                'me' => [
                    'name' => 'test',
                ],
            ],
        ];
        $handler = $this->mockHandler($history, [
            new Response(200, [], \GuzzleHttp\json_encode($data)),
            new Response(200, [], \GuzzleHttp\json_encode($data)),
            new Response(200, [], \GuzzleHttp\json_encode($data)),
            new Response(200, [], \GuzzleHttp\json_encode($data)),
        ]);
        $base_uri = 'http://testhost/graphql/';
        $service = $this->getService(compact('base_uri', 'handler'));

        $service->$method($graphql = '{me{name}}', $variables = $this->genData());
        $request = [
            'query' => $graphql,
            'variables' => $variables,
        ];
        $this->assertUniversal($requestMethod, $equalBody, $equalQuery, $history, 0, $data, $request);

        //async
        $service->$method($graphql, $variables, true)->wait();
        $this->assertUniversal($requestMethod, $equalBody, $equalQuery, $history, 1, $data, $request);

        //test stream/resource/file path
        $service->$method($filePath = __DIR__ . '/../../data/test.gql', $variables = $this->genData());

        $request = [
            'query' => file_get_contents($filePath),
            'variables' => $variables,
        ];
        $this->assertUniversal($requestMethod, $equalBody, $equalQuery, $history, 2, $data, $request);


    }
    public function testQuery() {
        $this->universal('query', 'GET', false, true);
    }

    public function testPquery() {
        $this->universal('pquery', 'POST', true, false);
    }

    public function testMutation() {
        $this->universal('mutation', 'POST', true, false);
    }

    /**
     * @inheritdoc
     */
    protected function getService($options)
    {
        return new GraphqlService($this->app, $options);
    }

    /**
     * @param $requestMethod
     * @param $equalBody
     * @param $equalQuery
     * @param $history
     * @param $data
     * @param $request
     */
    protected function assertUniversal($requestMethod, $equalBody, $equalQuery, $history, $index, $data, $request)
    {
        $this->assertEquals($requestMethod, Arr::get($history, $index . '.request')->getMethod());
        if ($equalBody) {
            $this->assertEquals($data, \GuzzleHttp\json_decode(Arr::get($history, $index . '.response')->getBody(), true));
        }
        if ($equalQuery) {
            $this->assertEquals(http_build_query($request), Arr::get($history, $index . '.request')->getUri()->getQuery());
        }
    }
}