<?php
/**
 * laravel-msi.
 * Date: 27/06/17
 * Time: 18:02
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */
namespace Tests;

use NavinLab\LaravelMsi\Facades\Msi;
use NavinLab\LaravelMsi\Facades\MsiRegistry;
use NavinLab\LaravelMsi\MsiServiceProvider;

class TestCase extends \Orchestra\Testbench\TestCase
{
    /**
     * @inheritdoc
     */
    protected function getPackageProviders($app)
    {
        return [MsiServiceProvider::class];
    }

    /**
     * @inheritdoc
     */
    protected function getPackageAliases($app)
    {
        return [
            'Msi' => Msi::class,
            'MsiRegistry' => MsiRegistry::class,
        ];
    }
}