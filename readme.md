# Laravel Micro Service Invoker
> The quick build service invoker for your microservices

Allows you to build services invokers/callers system

## Installing / Getting started

```shell
composer require navinlab/laravel-msi
```
Add code bellow into __config/app.php__ . 
```php
    'providers' => [
        ...
        NavinLab\LaravelMsi\MsiServiceProvider::class,
        ...
    ],
    'aliases' => [
        'Msi' => NavinLab\LaravelMsi\Facades\Msi::class,
    ],
```
Publish config file
```sh
    php artisan vendor:publish --provider="NavinLab\LaravelMsi\MsiServiceProvider"
```

Edit configuration file located in __config\msi.php__ file


## Developing

To start developing:

```shell
git clone https://bitbucket.org/navinlab/laravel-msi.git
cd laravel-msi/
composer install
./vendor/bin/phpunit
```

To use in Laravel project till developing, add into the project composer.json:
```json
  "repositories": [
    {
      "type": "path",
      "url": "<path>/laravel-msi",
      "options": {
        "symlink": true
      }
    }
  ],
```
Change the package composer.json version manually (should me grater that current):
```json
"version": "0.2.0",
```
And run composer update inside the project.

## Features

Based on guzzlehttp/guzzle package. Supports all guzzle features.

Wrapped services:
 - __simple__ - any rest or simple service
 - __rest__ - fullrest service  
 - __graphql__ - any graphql service
See cases bellow.

Supports middlewares:
 - __Localization__ - add to header current locale setting.
 - __CacheMiddleware__ - etag cache support 

## Contributing

As I use this for my own projects, I know this might not be the perfect approach
for all the projects out there. If you have any ideas, just
[open an issue][issues] and tell me what you think.

If you'd like to contribute, please fork the repository and make changes as
you'd like. Pull requests are warmly welcome.


## Licensing

This project is licensed under the MIT license.

## Documentation/Usage

#### Configuration
Configuration is located in __config/msi.php__ file and includes all cases covered Rest/Simple/Graphql examples.

Idea to separate heartbeat from source code and put all inside service configuration. Usually service is Guzzle client options. 

Below are examples covered many cases.

#### Usage
Short usage example:
```php
Msi::service($name = 'some-simple-service')->companies(1)->get();
//GET http://endpoint/api/v1/companies/1/
    
Msi::service($name = 'some-simple-service')->companies(1)->innovations()->get();
//GET http://endpoint/api/v1/companies/1/innovations/
    
Msi::service($name = 'some-simple-service')->companies(1)->innovations()->post($data = []);
Msi::service($name = 'some-rest-service')->companies(1)->innovations()->store($data);
//POST http://endpoint/api/v1/companies/1/innovations/
    
Msi::service($name = 'some-simple-service')->companies(1)->innovations(1)->put($data = []);
Msi::service($name = 'some-rest-service')->companies(1)->innovations(1)->update($data = []);
//PUT http://endpoint/api/v1/companies/1/innovations/1/
    
Msi::service($name = 'some-graphql-service')->query('path/name.gpl', $variables = []);
Msi::service($name = 'some-graphql-service')->query('{me{name}}', $variables = []);
    
Msi::service($name = 'some-graphql-service')->mutation('{mutate{name}}', $variables = []);
```

#### Simple service
Config:
```php
'services' => [
    ...
    'some-simple-service' => [
        'type' => 'simple',
        'base_uri' => 'http://somehost/api/v1/',
    ]
    ...
]
```
Routing syntax:
```php
Msi::service('some-simple-service')::method1(id1, ends = '', transformCamelGlue = null)->camelMethod2(id2, '', '-')->method3(null, '/')
```
Will provide uri
```
base_uri + method1/ + id1/ + camel-method2/ + id2 + method3/
```
To see the uri:
```php
$uri = Msi::service('some-simple-service')->route()->uri();
```
Example:
```php
Msi::service('some-simple-service')->companies(1)->get()
//GET http://somehost/api/v1/companies/1/
```
Methods:
```php
//GET request 
Msi::service('some-simple-service')->resources($id)->get($query = [], $async = false, $options = [])
Msi::service('some-simple-service')->resources()->get($query = [], $async = false, $options = [])
    
//POST request
Msi::service('some-simple-service')->resources()->post($json = [], $async = false, $options = [])
    
//PUT request
Msi::service('some-simple-service')->resources($id)->put($json = [], $async = false, $options = [])
    
//PATCH request
Msi::service('some-simple-service')->resources($id)->patch($json = [], $async = false, $options = [])
    
//DELETE request
Msi::service('some-simple-service')->resources($id)->delete($async = false, $options = [])
```
#### Rest service
Config:
```php
'services' => [
    ...
    'some-rest-service' => [
        'type' => 'rest',
        'base_uri' => 'http://somehost/api/v1/',
    ]
    ...
]
```
Has same routing logic as simple service and just has additional methods:
```php
//PUT request
Msi::service('some-rest-service')->resources($id)->update($json = [], $async = false, $options = [])
//same as
Msi::service('some-rest-service')->resources($id)->put($json = [], $async = false, $options = [])
    
//POST request
Msi::service('some-simple-service')->resources()->store($json = [], $async = false, $options = [])
//same as
Msi::service('some-simple-service')->resources()->post($json = [], $async = false, $options = [])
```
#### Graphql service
Config:
```php
'services' => [
    ...
    'some-graphql-service' => [
        'type' => 'graphql',
        'base_uri' => 'http://somehost/graphql',
    ]
    ...
]
```
Methods:
```php
$graphql = '{me{name}}';
//or
$grapqhl = resource_path('name.gpl');
    
//Query (GET request)
Msi::service('some-graphql-service')->query($graphql, $variables = [], $async = false, $options = []);
    
//Query (POST request)
Msi::service('some-graphql-service')->pquery($graphql, $variables = [], $async = false, $options = []);
    
//Mutation
Msi::service('some-graphql-service')->mutation($graphql, $variables = [], $async = false, $options = []);
```
#### Advanced options
The Service configuration is just Guzzle client options. You are freedom to fill any option.
Also you able to pass options into service method.
```php
Msi::service('some-service', $options = [])->...
```
#### Authorization example
Auth Basic example:
```php
return [
    ...
    'services' => [
        ...
        'some-service' => [
            'type' => 'rest',
            'auth' => ['username', 'password'],
        ],
        ...
    ],
    ...
]
```
Auth Digest example:
```php
return [
    ...
    'services' => [
        ...
        'some-service' => [
            'type' => 'rest',
            'auth' => ['username', 'password', 'digest'],
        ],
        ...
    ],
    ...
]
```
#### Global options
Sometimes you want to provide options for all you services. Put global options into options root.
```php
return [
    'options' => [
        'headers' => [
            'X-caller-id' => 123,
        ],
        //...same auth (bad example) for all services
        'auth' => ['username', 'password'],
    ],
];
```
#### Middleware
Some services we want to provie additional settings, that can't be added to configuration or you want to run some additional logic. 
For example, decode response body to object. You are free to write middleware. It is extended guzzle middleware.

To create the middleware extend the Middleware base class and override the request or response method
```php
namespace App\Msi\Middleware;
use NavinLab\LaravelMsi\Middleware\Middleware;

class MyAwesomeMiddleware extends Middleware 
{
    /**
     * @inheritdoc
     */
    public function request(RequestInterface $request, array $options)
    {
        $request->withHeader('X-some-header', 'value');
    }
    
    /**
     * @inheritdoc
     */
    public function response(ResponseInterface $response, array $options)
    {
        return $response;
    }
}
```
__Note:__ to run middleware methods we use App::make() and App::call() methods. 
So, you are able to inject additional dependecies you want. 

Now you can use it:
```php
Msi::service('some-service')
    ->middleware(\App\Msi\Middleware\MyAwesomeMiddleware::class)
    ->resources()->get();
```
Or from configuration:
```php
return [
    ...
    'services' => [
        'some-service' => [
            'type' => 'rest',
            'middleware' => [
                \App\Msi\Middleware\MyAwesomeMiddleware::class,
            ],
        ],
    ],
    ...
];
```
You also able to use standart guzzle middleware
```php
Msi::service('some-service')->handler()->push(Middleware::mapRequest());
Msi::service('some-service')->resources()->get();
```
__Note:__ middleware will be added to the end.
Better place to register standart or third party middleware is service provier.
And add to the configuration.
```php
class SomeServiceProvider extends ServiceProvider {
    public function register() {
        $this->app->singleton('msi.myawesome.middleware', function ($app) {
            return Middleware::mapRequest(function ($request) {
                ...do your logic here
            });
        })
    }
}
```
In code:
```php
Msi::service('some-service')->middleware('msi.myawesome.middleware')->resources()->get();
```
In configuration:
```php
return [
    'services' => [
        'some-service' => [
            'type' => 'rest',
            'middleware' => [
                ...
                'msi.myawesome.middleware',
                ...
            ],
        ],
    ],
];
```
#### Global Middleware
You can regiser global middleware for all services:
```php
return [
    'middleware' => [
        \NavinLab\LaravelMsi\Middleware\Localization::class,
    ],
];
```
#### CacheMiddleware
Please look at this [awesome cache middleware](https://github.com/Kevinrob/guzzle-cache-middleware) made by [Kevin](https://github.com/Kevinrob) .
Just wrap to use slim code style
```php
class CacheServiceProvider extends ServiceProvider 
{
    public function register() {
        $this->app->singleton('msi.cache.middleware', function () {
           return new CacheMiddleware(
               new PrivateCacheStrategy(
                 new LaravelCacheStorage(
                   Cache::store('redis')
                 )
               )
             ) 
        });
    }
} 
```
Now:
```php
Msi::service()->middleware('msi.cache.middleware')->route()->get();
```
Add to the config as first middleware:
```php
return [
    'middleware' => [
        'msi.cache.middleware',
        ...
    ],
];
```
#### Advanced usage
Access handler method:
```php
$middleware = ...;
Msi::service()->handler()->push($middleware)
Msi::service()->route()->get()
```
Override handler:
```php
Msi::service($name = null, $options)->route()->get()
```
#### Parallel/Asyncronous requests
Guzzle supports asyncronous request. 
Go over [this documentation](http://docs.guzzlephp.org/en/latest/quickstart.html) Async requests
```php
$promises = [
    'templates' => Msi::service('templates')->templates()->get(['type' => 1], true/*async*/),
    'orders' => Msi::service('orders')->orders()->get(['user_id' => 1], true/*async*/),
    'statistic' => Msi::service('statistic')->orders()->get(['user_id' => 1], true/*async*/),
];
// Wait on all of the requests to complete. Throws a ConnectException
// if any of the requests fail
$results = Promise\unwrap($promises);
    
// Wait for the requests to complete, even if some of them fail
$results = Promise\settle($promises)->wait();
```
#### Todo
- __Middleware condition__ improovement. Run middleware methods only if need
- __Response Middleware__ to generate Laravel correct Response. If service responsed with error we need to build the correct laravel response 
- __Tolerance Middleware__ catch all bad requests and do additional logic (inform/repeat/etc)
